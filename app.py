from flask import Flask, request
import pandas as pd

SORT_COLUMN = "net_generation"
DEFAULT_LIMIT = 10
DEFAULT_ORDER = 'DESC'
STATE_COLUMN = 'state'
SUCCESS = 200
BAD_REQUEST = 400
DATA_PATH = "data/cleaned_data.csv"


def load_data():
    """
        To load cleaned data from the disk
    """
    df = pd.read_csv(DATA_PATH)
    return df



### Initilize the flask app and load the load
app = Flask(__name__)
app.data = load_data()




@app.route("/plants", methods=['GET'])
def plants():
    """
        returns plants related data
        
        params:
        - limit: to sepcify number of results to be returned. Default value: 10
        - state: to filter by state.
        - order: sort order. Default value 'desc'

    """
    try:
        limit = int( request.args.get("limit", DEFAULT_LIMIT) ) 
        state = request.args.get("state", None)
        order = request.args.get("order", DEFAULT_ORDER)
        is_sort =  False if order.upper() == DEFAULT_ORDER else True
    except:
        return { "error" : "Bad Request"}, BAD_REQUEST
    
    if state is None:
        return { "data" : app.data.sort_values(by=SORT_COLUMN, ascending = is_sort ).iloc[:limit].to_dict('records') }, SUCCESS, {'ContentType':'application/json'}
    else:
        return { "data" : app.data[ app.data[STATE_COLUMN] == state.upper()].sort_values(by=SORT_COLUMN, ascending = is_sort ).iloc[:limit].to_dict('records') }, SUCCESS, {'ContentType':'application/json'}