from urllib import response
from app import app, load_data
import unittest
import json


class FlaskTestCase(unittest.TestCase):

    def test_load_data(self):
        data = load_data()
        self.assertEqual( data.shape, (12639, 6))

    def test_plants(self):
        tester = app.test_client(self)
        response = tester.get('/plants')
        self.assertEqual(response.status_code, 200)
    
    def test_plants_default(self):
        tester = app.test_client(self)
        response = tester.get('/plants')
        data = json.loads(response.text)
        self.assertEqual(len( data["data"] ), 10)

    def test_plants_default(self):
        tester = app.test_client(self)
        response = tester.get('/plants')
        data = json.loads(response.text)
        self.assertEqual(len( data["data"] ), 10)

    def test_plants_by_state(self):
        tester = app.test_client(self)
        response = tester.get('/plants?state=al')
        data = json.loads(response.text)
        passed = True
        for row in data["data"]:
            if row["state"].upper() == "AL":
                continue
            else:
                passed == False
                break
        self.assertTrue(passed)

    def test_plants_limit(self):
        tester = app.test_client(self)
        response = tester.get('/plants?limit=15')
        data = json.loads(response.text)
        self.assertEqual(len( data["data"] ), 15)
    
    def test_plants_state_limit(self):
        tester = app.test_client(self)
        response = tester.get('/plants?limit=15&state=al')
        data = json.loads(response.text)
        self.assertEqual(len( data["data"] ), 15)


if __name__ == "__main__":
    unittest.main()