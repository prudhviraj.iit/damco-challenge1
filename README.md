# Damco Challenge 1


## Stack
<b>Programming Language : </b> Python <br>
<b> HTTP Framework : </b>  Flask <br>
<b> Libraries : </b>  Pandas for data manipulation. Jupter Notebook for data exploration and clearning.


## Requirements & Installation
### Requirements
Python 3.x
### Installation

Clone the project. <br>
Change the directory into cloned project. <br>
Run pip install -r requirements.txt <br>
<b> To start : </b>flask run -p 6100



## API

#### Get plants - /plants 
By default returns top 10 plants ordered by net power generation <br>
<b> Example: </b> 
CLOUD URL : http://3.109.59.30:6100/plants<br>
LOCAL URL : http://localhost:6100/plants

###### params:

<b>limit : </b> To change the number of results returned <br>
<b>example : </b> 
CLOUD URL : http://3.109.59.30:6100/plants?limit=20<br>
LOCAL URL http://localhost:6100/plants?limit=20

<b>state : </b> To filter by state code <br>
<b>example :</b> 
CLOUD URL : http://3.109.59.30:6100/plants?limit=15&state=AK<br>
LOCAL URL : http://localhost:6100/plants?limit=15&state=AK

<b>order :</b> to sort. default sort order id DESC.



# DATA CLEANING 

Data cleaning is done in attached jupyter notebook. 
- Removed rows without any value in all  latitude, longitude and net power generation columns.
- Filled null values in net power generation column with 0
- Calculate required percentage values
- cleaned data is written into data folder as a csv file


# Unit Testing

Run : python tests.py







