from flask import Flask, request
import pandas as pd

SORT_COLUMN = "Plant annual net generation (MWh)"
DEFAULT_LIMIT = 10
DEFAULT_ORDER = 'DESC'
STATE_COLUMN = 'Plant state abbreviation'
SUCCESS = 200
BAD_REQUEST = 400


def load_data():
    df = pd.read_csv("data/cleaned_data.csv")
    #df.sort_values(by="Plant annual net generation (MWh)", ascending=False)
    return df

def load_state_data():
    df = load_data()
    total = df[SORT_COLUMN].sum()
    df["absolute_value"] = df.groupby(STATE_COLUMN, as_index=False)[SORT_COLUMN].transform("sum")
    filtered_df = df[[STATE_COLUMN, SORT_COLUMN, "absolute_value"]].groupby(STATE_COLUMN, as_index=False).first()
    filtered_df["percentage_value"] = ( filtered_df["absolute_value"] / total) * 100
    return filtered_df

app = Flask(__name__)
app.data = load_data()
app.state_data = load_state_data()



@app.route("/plants", methods=['GET'])
def get_plants():
    try:
        limit = int( request.args.get("limit", DEFAULT_LIMIT) ) 
        state = request.args.get("state", None)
        order = request.args.get("order", DEFAULT_ORDER)
        is_sort =  False if order.upper() == DEFAULT_ORDER else True
    except:
        return { "error" : "Bad Request"}, 400
    
    if state is None:
        return { "data" : app.data.iloc[:limit].sort_values(by=SORT_COLUMN, ascending = is_sort ).to_dict('records') }, 200, {'ContentType':'application/json'}
    else:
        return { "data" : app.data[ app.data[STATE_COLUMN] == state].iloc[:limit].sort_values(by=SORT_COLUMN, ascending = is_sort ).to_dict('records') }, 200, {'ContentType':'application/json'}


@app.route("/values", methods=['GET'])
def get_values():
    try:
        state = request.args.get("state", None)
    except:
        return { "error" : "Bad Request"}, 400
    
    if state is None:
        return { "data" : app.state_data.to_dict("records") } 
    else:
        return { "data" : app.state_data.to_dict("records") }